﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce2.Models
{
    public class ProductCreateViewModel
    {
        public List<Category> categories { get; set; }
        public Images images { get; set; }
        public Product product { get; set; }

        public HttpPostedFileBase [] files { get; set; }
    }
}