﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce2.Models
{
    public class ProductViewModel
    {
        public List<Product> products { get; set; }
        public List<Images> images { get; set;}
        public List<Category> categories { get; set; }

    }
}