﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce2.Models
{
    public class OrderDetailViewModel
    {
        public List<OrderDetail> orderDetails { get; set; }
        public Order order { get; set; }
        
    }
}