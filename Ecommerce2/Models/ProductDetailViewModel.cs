﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce2.Models
{
    public class ProductDetailViewModel
    {
        public List<Images> images { get; set; }
        public Product product { get; set; }


    }
}