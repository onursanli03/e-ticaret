﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce2.Models
{
    public class OrderViewModel
    {
        public Order order { get; set; }
        public OrderDetail orderDetails { get; set; }

        public List<Address> addresses { get; set; }

        public decimal Total { get; set; }

        public List<Cart> cartItems { get; set; }

        public Users user { get; set; }

    }
}