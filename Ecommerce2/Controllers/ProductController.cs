﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Net;

namespace Ecommerce2.Controllers
{
    public class ProductController : Controller
    {


        ecommerceEntities ent = new ecommerceEntities();
        
        public ActionResult Products(string id, string category)
        {


            var a = getProducts();
            var images = getImages();
            var categories = getCategories();
            ProductViewModel model = new ProductViewModel();
            model.images = images;
            model.categories = categories;
            model.products = a;
            int catID = 0;

            if (category == null && id == null)
            {
                return View(model);

            }
            if (string.IsNullOrEmpty(category))
            {
                id.Trim();
                a = ent.Product.Where(p => p.ProductName.Contains(id)).ToList();
            } else
            {
                category.Trim();
                catID = int.Parse(category);
                a = ent.Product.Where(p => p.CategoryID == catID).ToList();
            }
            if (category != "" && id != "")
            {
                id.Trim();
                category.Trim();
                catID = int.Parse(category);
                a = (from data in ent.Product
                     join data2 in ent.Category
                     on data.CategoryID equals data2.CategoryID
                     where data.ProductName.Contains(id) && data2.CategoryID == catID
                     select data).ToList();

            }else if (category == "" && id == "")
            {
                a = ent.Product.ToList();
            }
            model.products = a;
            return View(model);
        }


        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ProductDetailViewModel model = new ProductDetailViewModel();
            model.product = ent.Product.Find(id);
            model.images = ent.Images.Where(i=>i.ProductID == model.product.ProductID).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }
        
        public List<Category> getCategories()
        {
            var categories = ent.Category.ToList();

            return categories;
        }

        public List<Images> getImages()
        {
            var images = ent.Images.ToList();
            return images;
        }

        public List<Product> getProducts()
        {
            var products = ent.Product.ToList();
            return products;
        }
    }
}
