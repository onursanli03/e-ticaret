﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;

namespace Ecommerce2.Controllers
{
    public class AdminController : Controller
    {


        private ecommerceEntities ent = new ecommerceEntities();
        // GET: Admin
        [_AuthControl]
        public ActionResult Admin()
        {

            return View();

        }


        //PRODUCTS

        [_AuthControl]
        public ActionResult ListProduct(string searchString, string category)
        {
            ProductViewModel model = new ProductViewModel();
            var products = ent.Product.ToList();
            var categories = ent.Category.ToList();
            var images = ent.Images.ToList();
            model.images = images;
            model.categories = categories;
            model.products = products;
            int catID = 0;

            if (searchString == null && category == null)
            {
                return View(model);
            }
            if (string.IsNullOrEmpty(category))
            {
                searchString.Trim();
                products = ent.Product.Where(p => p.ProductName.Contains(searchString)).ToList();
            }
            else if (string.IsNullOrEmpty(searchString))
            {
                category.Trim();
                catID = int.Parse(category);
                products = ent.Product.Where(p => p.CategoryID == catID).ToList();
            }
            if (category != "" && searchString != "")
            {
                searchString.Trim();
                category.Trim();
                catID = int.Parse(category);
                products = (from data in ent.Product
                            join data2 in ent.Category
                            on data.CategoryID equals data2.CategoryID
                            where data.ProductName.Contains(searchString) && data2.CategoryID == catID
                            select data).ToList();

            }
            else if (category == "" && searchString == "")
            {
                products = ent.Product.ToList();
            }
            model.products = products;

            return View(model);

        }
        [_AuthControl]
        public ActionResult DetailProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductDetailViewModel model = new ProductDetailViewModel();
            var product = ent.Product.Find(id);
            var images = ent.Images.Where(i => i.ProductID == product.ProductID).ToList();

            model.product = product;
            model.images = images;
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [_AuthControl]
        public ActionResult CreateProduct()
        {
            var products = ent.Product.ToList();
            var categories = ent.Category.ToList();
            ProductCreateViewModel model = new ProductCreateViewModel();
            model.categories = categories;
            return View("CreateProduct", model);
        }
        // POST: Product/Create
        [HttpPost]
        public ActionResult CreateProduct(ProductCreateViewModel model, string category, HttpPostedFileBase[] files)
        {
            try
            {
                var categories = ent.Category.ToList();
                model.categories = categories;
                int categoryID = int.Parse(category);
                if (ModelState.IsValid)
                {
                    model.product.CategoryID = categoryID;
                    ent.Product.Add(model.product);
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            file.SaveAs(HttpContext.Server.MapPath("~/Content/Images/" + file.FileName));
                            string filename = file.FileName;
                            string[] imageCaption = filename.Split('.');
                            var image = new Images
                            {
                                ProductID = model.product.ProductID,
                                ImageCaption = imageCaption[0],
                                ImagePath = "~/Content/Images/" + file.FileName
                            };

                            model.images = image;
                            ent.Images.Add(model.images);
                        }
                    }
                    ent.SaveChanges();
                    return RedirectToAction("ListProduct", "Admin", "");
                }

                return View(model);
            }
            catch
            {
                return View();
            }
        }

        //GET 
        public ActionResult EditProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCreateViewModel model = new ProductCreateViewModel();
            model.product = ent.Product.Find(id);
            model.categories = ent.Category.ToList();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProduct(ProductCreateViewModel model, string category, HttpPostedFileBase[] files)
        {
            try
            {
                var categories = ent.Category.ToList();
                model.categories = categories;
                Product product = ent.Product.AsNoTracking().Where(p => p.ProductID == model.product.ProductID).FirstOrDefault();
                var images = ent.Images.AsNoTracking().Where(i => i.ProductID == product.ProductID).ToList();
                int catID = 0;
                if (string.IsNullOrEmpty(category))
                {
                    catID = product.CategoryID;
                }
                else
                {
                    catID = int.Parse(category);
                }
                if (ModelState.IsValid)
                {
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            file.SaveAs(HttpContext.Server.MapPath("~/Content/Images/" + file.FileName));
                            var image = new Images
                            {
                                ProductID = product.ProductID,
                                ImageCaption = file.FileName,
                                ImagePath = "~/Content/Images/" + file.FileName
                            };

                            model.images = image;
                            ent.Images.Add(model.images);
                        }
                    }
                    model.product.CategoryID = catID;
                    ent.Entry(model.product).State = System.Data.Entity.EntityState.Modified;
                    ent.SaveChanges();

                }
                return RedirectToAction("ListProduct", "Admin");
            }
            catch (DbUpdateException ex)
            {
                return View(ex);
            }
        }



        [_AuthControl]
        public ActionResult DeleteProduct(int id)
        {

            try
            {
                Product product = ent.Product.Find(id);
                var images = ent.Images.Where(i => i.ProductID == product.ProductID).ToList();
                foreach (var img in images)
                {
                    Images image = ent.Images.Where(i => i.ImageID == img.ImageID).FirstOrDefault();
                    ent.Images.Remove(image);
                }
                ent.Product.Remove(product);
                ent.SaveChanges();

                return RedirectToAction("ListProduct", "Admin", "");
            }
            catch
            {
                return View("Something went wrong !!(fk error posibility)");
            }
        }

        //CATEGORIES

        [_AuthControl]
        public ActionResult ListCategories(string searchString)
        {
            var category = ent.Category.ToList();

            if (string.IsNullOrEmpty(searchString))
            {
                return View(category);
            }
            else
            {
                category = ent.Category.Where(c => c.CategoryName.Contains(searchString)).ToList();

            }

            return View(category);
        }

        [_AuthControl]
        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ent.Category.Add(category);
                    ent.SaveChanges();
                    return RedirectToAction("ListCategories", "Admin");

                }
                return View(category);

            }
            catch
            {
                return View("Error occured when creating action !");
            }
        }

        public ActionResult EditCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = ent.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory(Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ent.Entry(category).State = System.Data.Entity.EntityState.Modified;
                    ent.SaveChanges();
                    return RedirectToAction("ListCategories", "Admin");
                }

                return View(category);
            }
            catch (DbUpdateException ex)
            {
                return View("error occured while updating element with id :" + category.CategoryID + ex);
            }
        }

        [_AuthControl]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                Category category = ent.Category.Find(id);
                ent.Category.Remove(category);
                ent.SaveChanges();
                return RedirectToAction("ListCategories", "Admin", "");
            }
            catch
            {
                return View("Something went wrong !!(fk error posibility)");
            }
        }

        //USERS

        [_AuthControl]
        public ActionResult ListUsers(string searchString)
        {
            var users = ent.Users.ToList();

            if (string.IsNullOrEmpty(searchString))
            {
                return View(users);
            }
            else
            {
                users = ent.Users.Where(u => u.Username.Contains(searchString) || u.Firstname.Contains(searchString) || u.Lastname.Contains(searchString)).ToList();
            }
            return View(users);
        }

        [_AuthControl]
        public ActionResult DeleteUser(int id)
        {

            try
            {
                Users user = ent.Users.Find(id);
                var adresses = ent.Address.Where(a => a.UserID == user.UserID).ToList();
                var orders = ent.Order.Where(o => o.UserID == user.UserID).ToList();
                foreach (var order in orders)
                {
                    OrderDetail orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == order.OrderID).FirstOrDefault();
                    ent.OrderDetail.Remove(orderDetails);
                }
                foreach (var a in orders)
                {
                    Order order = ent.Order.Where(o => o.OrderID == a.OrderID).FirstOrDefault();
                    ent.Order.Remove(order);
                }
                foreach (var b in adresses)
                {
                    Address adr = ent.Address.Where(a => a.AddressID == b.AddressID).FirstOrDefault();
                    ent.Address.Remove(adr);
                }
                ent.Users.Remove(user);
                ent.SaveChanges();
                return RedirectToAction("ListUsers", "Admin", "");

            }
            catch
            {
                return View("Somethint went wrong !!");
            }
        }
        [_AuthControl]
        public ActionResult CreateUser()
        {
            var roles = ent.Roles.ToList();
            UserCreateViewModel model = new UserCreateViewModel();
            model.roles = roles;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser(Users user, string roles)
        {

            int roleID = int.Parse(roles);

            try
            {
                if (ModelState.IsValid)
                {
                    user.RoleID = roleID;
                    ent.Users.Add(user);
                    ent.SaveChanges();
                    return RedirectToAction("ListUsers", "Admin");
                }
                return View(user);
            }
            catch
            {
                return View("Error occured while adding user");
            }
        }
        [_AuthControl]
        public ActionResult DetailUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users user = ent.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [_AuthControl]
        public ActionResult EditUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserCreateViewModel model = new UserCreateViewModel();
            model.user = ent.Users.Find(id);
            model.roles = ent.Roles.ToList();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(UserCreateViewModel model, string roles)
        {

            try
            {
                var rls = ent.Roles.ToList();
                model.roles = rls;
                model.user.ModifiedAt = DateTime.Now;
                int roleID = 0;
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(roles))
                    {
                        var user = ent.Users.AsNoTracking().Where(u => u.UserID == model.user.UserID).FirstOrDefault();
                        roleID = user.RoleID;
                    }
                    else
                    {
                        roleID = int.Parse(roles);
                    }
                    model.user.RoleID = roleID;
                    ent.Entry(model.user).State = System.Data.Entity.EntityState.Modified;
                    ent.SaveChanges();
                }
                return RedirectToAction("ListUsers", "Admin");
            }
            catch (DbUpdateException ex)
            {
                return View("Error occured while editing user with id " + model.user.UserID.ToString() + ex);
            }
        }
        //Orders
        [_AuthControl]
        public ActionResult ListOrders(string searchString)
        {
            var orders = ent.Order.ToList();

            if (string.IsNullOrEmpty(searchString))
            {
                return View(orders);
            }
            else
            {
                orders = ent.Order.Where(o => o.Users.Firstname.Contains(searchString) || o.Users.Lastname.Contains(searchString)).ToList();
            }

            return View(orders);
        }
        [_AuthControl]
        public ActionResult DetailOrder(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderDetailViewModel model = new OrderDetailViewModel();
            model.order = ent.Order.Find(id);
            model.orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == model.order.OrderID).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);

        }
        [_AuthControl]
        public ActionResult DeleteOrder(int id)
        {
            try
            {
                Order order = ent.Order.Find(id);
                var orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == order.OrderID).ToList();
                foreach (var ol in orderDetails)
                {
                    OrderDetail _ol = ent.OrderDetail.Where(o => o.OrderDetailID == ol.OrderDetailID).FirstOrDefault();
                    ent.OrderDetail.Remove(_ol);
                }
                ent.Order.Remove(order);
                ent.SaveChanges();
                return RedirectToAction("ListOrders", "Admin");
            }
            catch (DbUpdateException ex)
            {
                return View("Something went wrong while deleting order" + ex);
            }
        }

        //ADDRESS
        [_AuthControl]
        public ActionResult ListAddresses(string searchString)
        {
            var adr = ent.Address.ToList();

            if (string.IsNullOrEmpty(searchString))
            {
                return View(adr);
            }
            else
            {
                adr = ent.Address.Where(a => a.Users.Firstname.Contains(searchString) || a.Users.Lastname.Contains(searchString)).ToList();
            }

            return View(adr);

        }

        public ActionResult DeleteAddress(int id)
        {
            try
            {
                Address address = ent.Address.Find(id);

                var orders = ent.Order.Where(o => o.AddressID == address.AddressID).ToList();
                foreach (var order in orders)
                {
                    OrderDetail orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == order.OrderID).FirstOrDefault();
                    ent.OrderDetail.Remove(orderDetails);
                }
                foreach (var a in orders)
                {
                    Order order = ent.Order.Where(o => o.OrderID == a.OrderID).FirstOrDefault();
                    ent.Order.Remove(order);
                }

                ent.Address.Remove(address);
                ent.SaveChanges();
                return RedirectToAction("ListAddresses", "Admin");
            }
            catch (DbUpdateException ex)
            {
                return View("Something went wrong" + ex);
            }
        }

        [_AuthControl]
        public ActionResult EditAddress(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var address = ent.Address.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        [HttpPost]
        public ActionResult EditAddress(Address addr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ent.Entry(addr).State = System.Data.Entity.EntityState.Modified;
                    ent.SaveChanges();
                    return RedirectToAction("ListAddresses", "Admin", "");
                }

                return View(addr);
            }
            catch (DbUpdateException ex)
            {
                return View("Something went wrong !" + ex);
            }

        }

        [_AuthControl]
        public ActionResult ExportExcel(string searchString, string category)
        {
            var products = new List<Product>();
            int catID = 0;

            if (string.IsNullOrEmpty(category))
            {
                searchString.Trim();
                products = ent.Product.Where(p => p.ProductName.Contains(searchString)).ToList();
            }
            else if (string.IsNullOrEmpty(searchString))
            {
                category.Trim();
                catID = int.Parse(category);
                products = ent.Product.Where(p => p.CategoryID == catID).ToList();
            }
            if (category != "" && searchString != "")
            {
                searchString.Trim();
                category.Trim();
                catID = int.Parse(category);
                products = (from data in ent.Product
                            join data2 in ent.Category
                            on data.CategoryID equals data2.CategoryID
                            where data.ProductName.Contains(searchString) && data2.CategoryID == catID
                            select data).ToList();

            }
            else if (category == "" && searchString == "")
            {
                products = ent.Product.ToList();
            }
            int count = 2;

            using (ExcelPackage pckg = new ExcelPackage())
            {
                ExcelWorksheet workSheet = pckg.Workbook.Worksheets.Add("Products");
                workSheet.Cells["A1"].Value = "Product Brand";
                workSheet.Cells["B1"].Value = "Category";
                workSheet.Cells["C1"].Value = "Product Name";
                workSheet.Cells["D1"].Value = "Product Description";
                workSheet.Cells["E1"].Value = "Product Price";
                workSheet.Cells["F1"].Value = "Product Color";

                using (var rng = workSheet.Cells["A1:F1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Font.Color.SetColor(Color.White);
                    rng.Style.WrapText = true;
                    rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.Black);
                }

                foreach (var items in products)
                {

                    workSheet.Cells[count, 1].Value = items.ProductBrand;
                    workSheet.Cells[count, 2].Value = items.Category.CategoryName;
                    workSheet.Cells[count, 3].Value = items.ProductName;
                    workSheet.Cells[count, 4].Value = items.ProductDesc;
                    workSheet.Cells[count, 5].Value = items.ProductPrice;
                    workSheet.Cells[count, 6].Value = items.ProductColor;
                    count++;
                }

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment: filename=" + "ProductExport.xlsx");
                Response.BinaryWrite(pckg.GetAsByteArray());
                Response.End();
            }

            return View();
        }

        [_AuthControl]
        public ActionResult ImportExcel()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportExcel(ImportExcelViewModel model, HttpPostedFileBase uploadedFile)
        {

            string[] names = uploadedFile.FileName.Split('.');
            string filename = names[0];

            if (uploadedFile != null && uploadedFile.ContentLength > 0)
            {

                using (ExcelPackage pckg = new ExcelPackage(uploadedFile.InputStream))
                {
                    var extension = Path.GetExtension(uploadedFile.FileName);
                    var excelFile = Path.Combine(Server.MapPath("~/Content/Excels"), filename + extension);
                    if (System.IO.File.Exists(excelFile))
                    {
                        System.IO.File.Delete(excelFile);
                    }
                    else if (uploadedFile.ContentType == "application/vdn.ms-excel" || uploadedFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        uploadedFile.SaveAs(excelFile);
                        FileInfo eFile = new FileInfo(excelFile);
                        using (ExcelPackage package = new ExcelPackage(eFile))
                        {
                            if (!eFile.Name.EndsWith("xlsx"))
                            {
                                ModelState.AddModelError("", "Incompartible Excel Document. Please use MSExcel 2007 and Above!");
                            }
                            else
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                                if (workSheet == null)
                                {
                                    ModelState.AddModelError("", "Wrong Excel Format!");
                                }
                                else
                                {
                                    var firstRow = workSheet.Dimension.Start.Row + 1;
                                    var firstColumn = workSheet.Dimension.Start.Column;
                                    var lastRow = workSheet.Dimension.End.Row;
                                    var lastColumn = workSheet.Dimension.End.Column;
                                    while (firstColumn != lastColumn)
                                    {
                                        var range = workSheet.Cells[firstRow, firstColumn, lastRow, lastColumn];
                                        if (range.Any(c => c.Value != null))
                                        {
                                            break;
                                        }
                                        firstRow++;
                                    }

                                    for (int row = 2; row <= lastRow; row++)
                                    {
                                        var product = new Product
                                        {
                                            ProductBrand = workSheet.Cells[row, 1].Value.ToString(),
                                            ProductName = workSheet.Cells[row, 3].Value.ToString(),
                                            ProductDesc = workSheet.Cells[row, 4].Value.ToString(),
                                            ProductPrice = decimal.Parse(workSheet.Cells[row, 5].Value.ToString()),
                                            ProductColor = workSheet.Cells[row, 6].Value.ToString(),

                                        };
                                        var categoryName = workSheet.Cells[row, 2].Value.ToString();
                                        Category category = ent.Category.Where(c => c.CategoryName == categoryName).FirstOrDefault();
                                        product.CategoryID = category.CategoryID;
                                        
                                        ent.Product.Add(product);
                                    }
                                    try
                                    {
                                        ent.SaveChanges();
                                    }
                                    catch (Exception Ex)
                                    {
                                        throw Ex;
                                    }
                                }
                            }
                        }
                    }
                    ViewBag.Message = "Excel Successfully imported !! ";
                    return RedirectToAction("ListProduct", "Admin");
                }
            }
            ViewBag.Message = "Your Excel file was not uploaded. Ensure you upload an excel workbook file.";
            return View(model);
        }

    }
}