﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Net;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Web.Security;

namespace Ecommerce2.Controllers
{
    public class UserController : Controller
    {


        private ecommerceEntities ent = new ecommerceEntities();

        [_SessionControl]
        //GET: User
        public ActionResult Users()
        {
            return View(ent.Users.ToList());
        }


        [_SessionControl]
        //GET: User/Details/5
        public ActionResult Details()
        {
            if (User.Identity.IsAuthenticated) {

                Users user = ent.Users.FirstOrDefault(u => u.Username.Equals(User.Identity.Name.ToString()));

                if (user.UserID == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var usr = ent.Users.Find(user.UserID);

                if(usr == null)
                {
                    return HttpNotFound();
                }

                return View(usr);
            }

           
            return RedirectToAction("Home/index");

        }
       
        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,Username,Password,Firstname,Lastname,Phone,Email,CreatedAt,ModifiedAt")]Users user,LoginModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    ent.Users.Add(user);
                    ent.SaveChanges();

                    var users = ent.Users.Where(data => data.Username == model.Username && data.Password == data.Password);

                    if (users.Count()>0)
                    {
                        FormsAuthentication.SetAuthCookie(model.Username, true);
                        return RedirectToAction("Index", "Home");
                    }
                    
                }
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }

            return View(model);
        }

        [_SessionControl]
        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users user = ent.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,Username,Password,Firstname,Lastname,Phone,Email,CreatedAt,ModifiedAt")]Users user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    ent.Entry(user).State = EntityState.Modified;
                    user.ModifiedAt = DateTime.Now;
                    ent.SaveChanges();
                    return RedirectToAction("Products","Product","");
                }
                return View(user);

            }
            catch
            {
                return View();
            }
        }
        [_SessionControl]
        // GET: User/Delete/5
        public ActionResult Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users user = ent.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("DELETE")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Users user = ent.Users.Find(id);
                ent.Users.Remove(user);
                ent.SaveChanges();
                return RedirectToAction("Users");
            }
            catch
            {
                return View();
            }
        }
    }
}
