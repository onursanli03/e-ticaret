﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecommerce2.Models;
using System.Web.Mvc;
using System.Web.Security;

namespace Ecommerce2.Controllers
{
    public class _AuthControlAttribute : ActionFilterAttribute
    {
        private ecommerceEntities ent = new ecommerceEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Users user = ent.Users.FirstOrDefault(u => u.Username == HttpContext.Current.User.Identity.Name);

                if (user.RoleID != 1)
                {
                    FormsAuthentication.SignOut();
                   filterContext.HttpContext.Session.Abandon();
                    filterContext.HttpContext.Response.Redirect("/Login/Login");
                }
               
            }
            else
            {
                filterContext.HttpContext.Response.Redirect("/Login/Login");
            }
            

        }




    }
}