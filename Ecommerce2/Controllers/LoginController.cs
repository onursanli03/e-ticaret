﻿using Ecommerce2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Ecommerce2.Controllers
{
    public class LoginController : Controller
    {
        private ecommerceEntities ent = new ecommerceEntities();


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (String.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                FormsAuthentication.SignOut();
                return View();
            }
            return Redirect("Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var users = ent.Users.Where(data => data.Username == model.Username && data.Password == model.Password);

                if (users.Count() > 0)
                {

                    Users usr = ent.Users.First(u => u.Username == model.Username);
                    if (usr.RoleID == 1)
                    {
                        FormsAuthentication.SetAuthCookie(model.Username, true);
                        return RedirectToAction("Admin", "Admin");
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(model.Username, true);
                        return RedirectToAction("Products", "Product");
                    }


                }
                else
                {
                    ModelState.AddModelError("", "Wrong Username or Password !");
                }

            }

            return View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Login");
        }

    }
}