﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Data.Entity.Infrastructure;
using System.Net;
using Ecommerce2.Controllers;

namespace Ecommerce2.Models
{
    public class OrderController : Controller
    {
        
        private ecommerceEntities ent = new ecommerceEntities();
        [_SessionControl]
        public ActionResult Orders()
        {
            var orders = getOrders();

            return View(orders);
        }
        [_SessionControl]
        public ActionResult OrderDetail(int? id,OrderDetailViewModel model)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = ent.Order.Where(o => o.OrderID == id).FirstOrDefault();
            var orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == id).ToList();

            model.order = order;
            model.orderDetails = orderDetails;
            
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }
        [_SessionControl]
        // GET: Order
        public ActionResult CreateOrder()
        {

            List<Cart> carts = listCartItems();
            Users user = getUser();
            decimal total = getTotal();
            var a = listAddress();
            OrderViewModel ord = new OrderViewModel();
            ord.addresses = a;
            ord.cartItems = carts;
            ord.user = user;
            ord.Total = total;
            return View("CreateOrder", ord);

        }


        [HttpPost]
        public ActionResult CreateOrder(Order order,string Address)
        {
            int adrID = int.Parse(Address);
            OrderViewModel model = new OrderViewModel();
            List<Cart> cartItems = listCartItems();
            Users user = getUser();
            Address adr = ent.Address.FirstOrDefault(a=>a.AddressID == adrID);
            order.UserID = user.UserID;
            order.Total = getTotal();
            order.AddressID = adrID;
            ent.Order.Add(order);
 
            foreach (var items in cartItems)
            {
                var orderDetail = new OrderDetail
                {
                    OrderID = order.OrderID,
                    ProductID = items.ProductID,
                    Quantity = items.Count,
                    UnitPrice = items.Product.ProductPrice
                };
                model.orderDetails = orderDetail;
                ent.OrderDetail.Add(orderDetail);
            }

            ent.SaveChanges();
            ent.removeCart(user.Username);
            ent.SaveChanges();
            model.cartItems = cartItems;
            model.order = order;
            model.Total = order.Total;

            return View("Complete", model);
        }

        private decimal getTotal()
        {
            var usr = User.Identity.Name.ToString();
            decimal? total = (from cartItems in ent.Cart
                              where cartItems.CartID == usr
                              select (int?)cartItems.Count * cartItems.Product.ProductPrice).Sum();
            return total ?? decimal.Zero;
        }

        private List<Cart> listCartItems()
        {
            var usr = User.Identity.Name.ToString();
            List<Cart> carts = (from data in ent.Cart
                                join data2 in ent.Users on data.CartID equals data2.Username
                                join data3 in ent.Product on data.ProductID equals data3.ProductID
                                where data2.Username.Equals(usr)
                                select data).ToList();

            return carts;
        }

        private List<Address> listAddress()
        {
            var usr = User.Identity.Name.ToString();
            Users usrID = ent.Users.FirstOrDefault(u => u.Username == usr);
            var a = ent.Address.Where(u => u.UserID == usrID.UserID).ToList();
            return a;

        }

        private Users getUser()
        {
            var usr = User.Identity.Name.ToString();

            return ent.Users.FirstOrDefault(u => u.Username == usr);


        }
        private List<Order> getOrders()
        {
            var usr = User.Identity.Name.ToString();
            Users user = ent.Users.FirstOrDefault(u => u.Username == usr);
            var order = ent.Order.Where(o => o.UserID == user.UserID).ToList();
            return order;
        }


    }
}