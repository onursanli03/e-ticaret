﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Net;

namespace Ecommerce2.Controllers
{
    public class CartController : Controller
    {
        private ecommerceEntities ent = new ecommerceEntities();

        [_SessionControl]
        public ActionResult Payment()
        {
            List<Cart> carts = listCartItems();
            if (carts.Count == 0)
            {
                return RedirectToAction("Index", "Cart");
            }
            return RedirectToAction("CreateOrder", "Order", "");
        }

        [_SessionControl]
        // GET: Cart
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                List<Cart> carts = listCartItems();
                var a = listAddress();
                CartViewModel model = new CartViewModel();
                model.Total = getTotal();
                model.cartItems = carts;

                return View(model);
            }
            return RedirectToAction("Login", "Login");
        }
        [_SessionControl]
        public ActionResult AddChart(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var addedProduct = ent.Product.Single(p => p.ProductID == id);

                var cartItem = ent.Cart.SingleOrDefault(
                    c => c.CartID == User.Identity.Name.ToString()
                    && c.ProductID == addedProduct.ProductID);

                if (cartItem == null)
                {
                    cartItem = new Cart
                    {
                        ProductID = addedProduct.ProductID,
                        CartID = User.Identity.Name.ToString(),
                        Count = 1,
                        CreatedAt = DateTime.Now
                    };
                    ent.Cart.Add(cartItem);
                }
                else
                {
                    cartItem.Count++;
                }

                try
                {
                    ent.SaveChanges();
                    return RedirectToAction("Products", "Product", "");
                }
                catch (DbUpdateException ex)
                {
                    throw ex;
                }
            }
            return RedirectToAction("Login","Login","");

        }

        [_SessionControl]
        public ActionResult RemoveFromCart(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart crt = ent.Cart.Find(id);
            if (crt == null)
            {
                return HttpNotFound();
            }
            ent.removeFromCart(id);
            ent.SaveChanges();
            return RedirectToAction("Index", "Cart", "");
        }


        private decimal getTotal()
        {
            var usr = User.Identity.Name.ToString();
            decimal? total = (from cartItems in ent.Cart
                              where cartItems.CartID == usr
                              select (int?)cartItems.Count * cartItems.Product.ProductPrice).Sum();
            return total ?? decimal.Zero;
        }


        private List<Cart> listCartItems()
        {
            if (User.Identity.IsAuthenticated)
            {
                var username = User.Identity.Name.ToString();

                List<Cart> carts = (from data in ent.Cart
                                    join data2 in ent.Users on data.CartID equals data2.Username
                                    join data3 in ent.Product on data.ProductID equals data3.ProductID
                                    where data2.Username.Equals(username)
                                    select data).ToList();

                return carts;
            }

            return null;

        }

        private List<Address> listAddress()
        {
            if (User.Identity.IsAuthenticated)
            {
                var username = User.Identity.Name.ToString();
                Users usrID = ent.Users.FirstOrDefault(u => u.Username == username);
                var a = ent.Address.Where(u => u.UserID == usrID.UserID).ToList();
                return a;
            }
            return null;
        }

        private Users getUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var usr = User.Identity.Name.ToString();
                return ent.Users.FirstOrDefault(u => u.Username == usr);

            }

            return null;

        }

    }
}
