﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce2.Models;
using System.Net;

using System.Data.Entity;


namespace Ecommerce2.Controllers
{
    public class AddressController : Controller
    {

        ecommerceEntities ent = new ecommerceEntities();

        [_SessionControl]
        // GET: Address
        public ActionResult Address()
        {
            if (User.Identity.IsAuthenticated)
            {
                var usr = ent.Users.FirstOrDefault(u => u.Username.Equals(User.Identity.Name.ToString()));
                var adr = ent.Address.Where(a=>a.UserID == usr.UserID).ToList();
                if (usr.UserID == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else if (usr == null || adr == null)
                {
                    return HttpNotFound();
                }
                return View(adr);
            }
            return View("Something Went Wrong !!");
        }
        [_SessionControl]
        // GET: Address/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Address adr = ent.Address.Find(id);

            if (adr == null)
            {
                return HttpNotFound();
            }

            return View(adr);
        }

        // GET: Address/Create
        public ActionResult Create()
        {
            return View();
        }
        [_SessionControl]
        // POST: Address/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include ="AddressID,UserID,City,Country,State,PostalCode,AddressDesc")]Address adr)
        {
            try
            {

                var usr = ent.Users.FirstOrDefault(u => u.Username.Equals(User.Identity.Name.ToString()));
                if (ModelState.IsValid)
                {
                    adr.UserID = usr.UserID;
                    ent.Address.Add(adr);
                    ent.SaveChanges();
                    return RedirectToAction("Address", "Address", "");
                }
                return View(adr);
            }
            catch
            {
                return View("Something went wrong with " + adr.ToString());
            }
        }
        [_SessionControl]
        // GET: Address/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address adr = ent.Address.Find(id);
            if (adr == null)
            {
                return HttpNotFound();
            }
            
            return View(adr);
        }

        // POST: Address/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AddressID,UserID,City,Country,State,PostalCode,AddressDesc")]Address address)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ent.Entry(address).State = EntityState.Modified;
                    ent.SaveChanges();
                    return RedirectToAction("Address", "Address", "");
                }
              
                return View(address);

            }
            catch
            {
                return View("Something went Wrong !!");
            }
        }

        [_SessionControl]
        // POST: Address/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                Address adr = ent.Address.Find(id);
                var orders = ent.Order.Where(o => o.AddressID == adr.AddressID).ToList();
                foreach (var order in orders)
                {
                    OrderDetail orderDetails = ent.OrderDetail.Where(ol => ol.OrderID == order.OrderID).FirstOrDefault();
                    ent.OrderDetail.Remove(orderDetails);
                }
                foreach (var a in orders)
                {
                    Order order = ent.Order.Where(o => o.OrderID == a.OrderID).FirstOrDefault();
                    ent.Order.Remove(order);
                }
                ent.Address.Remove(adr);
                ent.SaveChanges();

                return RedirectToAction("Address","Address","");
            }
            catch
            {
                return View("Something went wrong !!(fk error possiblity)");
            }
        }
    }
}
