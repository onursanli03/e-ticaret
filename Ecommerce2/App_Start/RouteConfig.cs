﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ecommerce2.Models;

namespace Ecommerce2
{
    public class RouteConfig
    {

        private static ecommerceEntities ent = new ecommerceEntities();
        public static void RegisterRoutes(RouteCollection routes)
        {


            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                            name: "Default",
                            url: "{controller}/{action}/{id}",
                            defaults: new { controller = "Product", action = "Products", id = UrlParameter.Optional }
                        );



        }
    }
}
